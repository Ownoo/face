﻿using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Globalization;

namespace Face
{
    public partial class Form : System.Windows.Forms.Form
    {
        public Form()
        {
            InitializeComponent();
            var backgroundWorker = new BackgroundWorker
            {
                WorkerReportsProgress = true,
                WorkerSupportsCancellation = true
            };
            backgroundWorker.DoWork += BackgroundWorkerOnDoWork;
            backgroundWorker.ProgressChanged += BackgroundWorkerOnProgressChanged;
            backgroundWorker.RunWorkerAsync();
        }

        private static void BackgroundWorkerOnProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            var userObject = e.UserState;
            var percentage = e.ProgressPercentage;
        }

        private void BackgroundWorkerOnDoWork(object sender, DoWorkEventArgs e)
        {
            var worker = (BackgroundWorker)sender;
            while (!worker.CancellationPending)
            {
                textBox1.Text = DateTime.Now.ToString(CultureInfo.CurrentCulture);
                CaptureFrame();
            }
        }

        private void CaptureFrame()
        {
            var capture = new VideoCapture(0);
            using (var mat = new Mat())
            {
                capture.Read(mat);
                if (!mat.IsEmpty)
                {
                    var processed = Detect(mat, out var found);
                    pictureBox1.Image = new Bitmap(processed.Bitmap);
                }
            }
        }

        private static Mat Detect(Mat mat, out bool found)
        {
            found = false;
            using (var eyeClassifier = new CascadeClassifier("haarcascade_eye.xml"))
            using (var faceClassifier = new CascadeClassifier("haarcascade_frontalface_default.xml"))
            {
                using (var ugray = new UMat())
                {
                    CvInvoke.CvtColor(mat, ugray, ColorConversion.Bgr2Gray);
                    CvInvoke.EqualizeHist(ugray, ugray);
                    // detect faces in image
                    var faces = faceClassifier.DetectMultiScale(ugray, 1.1, 10, new Size(20, 20));
                    foreach (var face in faces)
                    {
                        //detect eyes
                        using (var faceRegion = new UMat(ugray, face))
                        {
                            var eyes = eyeClassifier.DetectMultiScale(faceRegion, 1.1, 10, new Size(20, 20));
                            if (eyes.Length > 1)
                            {
                                foreach (var eye in eyes)
                                {
                                    var eyeRectangle = eye;
                                    eyeRectangle.Offset(face.X, face.Y);
                                    // mark up eyes on image
                                    CvInvoke.Rectangle(mat, eyeRectangle, new Bgr(Color.Green).MCvScalar, 2);
                                }
                                // mark up face on image
                                CvInvoke.Rectangle(mat, face, new Bgr(Color.Yellow).MCvScalar, 2);
                                found = true;
                            }
                        }
                    }
                }
            }
            return mat;
        }
    }
}
